root = "/home/rahul/apps/missed_call_alert/current"
working_directory root
pid "#{root}/tmp/pids/unicorn.pid"
stderr_path "#{root}/log/unicorn.log"
stdout_path "#{root}/log/unicorn.log"

listen "/tmp/unicorn.missed_call_alert.sock"
worker_processes 2
timeout 30
