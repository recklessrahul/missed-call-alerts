rails_env   = ENV['RAILS_ENV']  || "production"
rails_root  = ENV['RAILS_ROOT'] || "/home/rahul/apps/missed_call_alert/current"
num_workers = rails_env == 'production' ? 2 : 2

num_workers.times do |num|
  God.watch do |w|
    w.dir      = "#{rails_root}"
    w.name     = "resque-#{num}"
    w.group    = 'rails_app'
    w.interval = 30.seconds
    w.env      = {"QUEUE"=>"*", "RAILS_ENV"=>rails_env}
    w.start    = "/home/rahul/.rvm/bin/bootup_rake -f #{rails_root}/Rakefile environment resque:work"


    # restart if memory gets too high
    w.transition(:up, :restart) do |on|
      on.condition(:memory_usage) do |c|
        c.above = 350.megabytes
        c.times = 2
      end
    end

    # determine the state on startup
    w.transition(:init, { true => :up, false => :start }) do |on|
      on.condition(:process_running) do |c|
        c.running = true
      end
    end

    # determine when process has finished starting
    w.transition([:start, :restart], :up) do |on|
      on.condition(:process_running) do |c|
        c.running = true
        c.interval = 5.seconds
      end

      # failsafe
      on.condition(:tries) do |c|
        c.times = 5
        c.transition = :start
        c.interval = 5.seconds
      end
    end

    # start if process is not running
    w.transition(:up, :start) do |on|
      on.condition(:process_running) do |c|
        c.running = false
      end
    end
  end
end


God.watch do |w|
  w.dir      = "#{rails_root}"
  w.name     = "faye"
  w.group    = 'rails_app'
  w.interval = 30.seconds
  w.env      = {"QUEUE"=>"*", "RAILS_ENV"=>rails_env}
  w.start    = "/home/rahul/.rvm/bin/bootup_thin start -R #{rails_root}/private_pub.ru -p 9292"


  # restart if memory gets too high
  w.transition(:up, :restart) do |on|
    on.condition(:memory_usage) do |c|
      c.above = 350.megabytes
      c.times = 2
    end
  end

  # determine the state on startup
  w.transition(:init, { true => :up, false => :start }) do |on|
    on.condition(:process_running) do |c|
      c.running = true
    end
  end

  # determine when process has finished starting
  w.transition([:start, :restart], :up) do |on|
    on.condition(:process_running) do |c|
      c.running = true
      c.interval = 5.seconds
    end

    # failsafe
    on.condition(:tries) do |c|
      c.times = 5
      c.transition = :start
      c.interval = 5.seconds
    end
  end

  # start if process is not running
  w.transition(:up, :start) do |on|
    on.condition(:process_running) do |c|
      c.running = false
    end
  end
end

APP_PATH = "/home/rahul/apps/mca"
PID_FILE = "#{APP_PATH}/../shared/pids/adhearsion.pid"

def ahnctl_command(action = 'start')
  "/home/rahul/.rvm/bin/bootup_ahn #{action} #{APP_PATH} --pid-file=#{PID_FILE}"
end

God.watch do |w|
  w.name = "mca-adhearsion"
  w.group = "mca"

  w.interval = 30.seconds
  w.start_grace = 20.seconds
  w.restart_grace = 20.seconds

  w.dir = APP_PATH
  w.env      = {"AHN_ENV"=>"production", "RAILS_ENV"=>"production"}

  w.start = ahnctl_command 'start'
  w.stop = ahnctl_command 'stop'
  w.restart = ahnctl_command 'restart'

  w.pid_file = PID_FILE
  w.behavior :clean_pid_file

  w.start_if do |start|
    start.condition(:process_running) do |c|
      c.interval = 5.seconds
      c.running = false
    end
  end

  w.restart_if do |restart|
    restart.condition(:memory_usage) do |c|
      c.above = 150.megabytes
      c.times = [3, 5] # 3 out of 5 intervals
    end

    restart.condition(:cpu_usage) do |c|
      c.above = 50.percent
      c.times = 5
    end
  end

  w.lifecycle do |on|
    on.condition(:flapping) do |c|
      c.to_state = [:start, :restart]
      c.times = 5
      c.within = 5.minute
      c.transition = :unmonitored
      c.retry_in = 10.minutes
      c.retry_times = 5
      c.retry_within = 2.hours
    end
  end
end

