MissedCall::Application.routes.draw do


  get "pages/home"
  root :to => 'pages#home'

  resources :calls
  resources :user_sessions, as: :user_sessions
  resources :users, as: :users
    
  match 'login' => 'user_sessions#new', :as => :login
  match 'logout' => 'user_sessions#destroy', :as => :logout
  match 'log_a_call' => 'user_sessions#log_a_call', as: :log_a_call
  mount Resque::Server, :at => "/resque"
end
