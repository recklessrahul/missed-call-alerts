class Call < ActiveRecord::Base
  attr_accessible :person_mobile, :remarks, :status, :call_type, :person_attributes, :user_id
  belongs_to :person, foreign_key: :person_mobile, primary_key: :person_mobile
  belongs_to :user
  default_scope order("updated_at desc")
  accepts_nested_attributes_for :person
  validates_presence_of :remarks, on: :update

  include Tire::Model::Search
  include Tire::Model::Callbacks

  mapping do
    indexes :id, type: 'integer'
    indexes :user_id, type: 'integer'
    indexes :person_mobile
    indexes :remarks
    indexes :status
    indexes :call_type
    indexes :created_at, type: 'date'
    indexes :updated_at, type: 'date'
    indexes :name, boost: 5
    indexes :company, boost: 4
    indexes :email
    indexes :mobile
    indexes :closed_by
    indexes :state
  end

  def self.search(params)
    tire.search(page: params[:page], per_page: 7) do
      query { string params[:query]} if params[:query].present?
      if params[:from].present? and params[:to].present?
        filter :range, created_at: {from: params[:from], to: params[:to]}
      end
      sort { by :updated_at, "desc" } if params[:query].blank?
    end
  end

  def to_indexed_json
    to_json(methods: [:mobile, :name, :company, :email, :closed_by, :state])
  end

  def mobile
    person_mobile
  end

  def closed_by
    user.try(:username)
  end

  def name
    person.try(:person_name)
  end

  def company
    person.try(:institution_name)
  end

  def email
    person.try(:person_email)
  end

  def state
    person.try(:person_state)
  end
end
