require 'open-uri'
class Person < ActiveRecord::Base
  attr_accessible :institution_name, :person_mobile, :person_name, :person_email, :person_state
  has_many :calls, foreign_key: :person_mobile, primary_key: :person_mobile
  validates_uniqueness_of :person_mobile
  validates_presence_of :person_name, :institution_name

  include Tire::Model::Search
  include Tire::Model::Callbacks

  def self.lookup number
    @person = Person.find_by_person_mobile(number)
    unless @person
      @data = dar_lookup(number)
      if @data
        @person = Person.create(person_name: @data["name"], person_mobile: number, institution_name: "Forbes Technosys Ltd.", person_email: @data["email"], person_state: @data["state"])
      end
    end
    @person
  end

  def self.dar_lookup number
    uri_string = "http://173.201.20.182:8080/engineer_info.json?username=" + number.to_s
    uri = URI.parse(uri_string) 
    response = uri.open.read.split("\(")[1].split("\)")[0] rescue nil
    data = JSON.parse(response).to_hash rescue nil
  end
end
