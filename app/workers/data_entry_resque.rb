require 'erb'

class DataEntryResque
  extend ActionView::Helpers::TextHelper
  @queue = :entry

  def self.perform mobile, type
    @person = get_person(mobile)
    missed_call = Call.create(person_mobile: mobile, status: "pending", call_type: type)
    path = File.expand_path("../../views/users/_table_row.html.erb", __FILE__)
    content = File.read path
    call = missed_call
    renderer = ERB.new(content)
    calls_count = Call.where(status: "pending").count
    case calls_count
    when 0..5
      status = "text text-success span12 center call_count"
    when 6..10
      status = "text text-warning span12 center call_count"
    else
      status = "text text-error span12 center call_count"
    end

    calls_count_string = pluralize(calls_count, "Pending Call")
    PrivatePub.publish_to("/calls/#{type}", html: renderer.result(binding), alert: "New Missed Call from #{missed_call.person_mobile}", call_count: calls_count_string, call_class: status)
    puts("/calls/#{type}")
  end

  def self.get_person number
    Person.lookup(number)
  end
end
