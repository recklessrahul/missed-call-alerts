class CallsController < ApplicationController
  include ActionView::Helpers::TextHelper
  def index
    begin
      @calls = Call.search(params)
    rescue Tire::Search::SearchRequestFailed
      @calls = Call.paginate(:page => params[:page], per_page: 7)
      flash[:error] = "Invalid Query. Please try again"
    end
  end

  def update
    @call = Call.find params[:id]
    unless @call.status == "pending"
      redirect_to user_path(current_user), alert: "This call has already been closed!"
      return
    end
    if @call.update_attributes(params[:call])
      calls_count = Call.where(status: "pending").count
      case calls_count
      when 0..5
        status = "text text-success span12 center call_count"
      when 6..10
        status = "text text-warning span12 center call_count"
      else
        status = "text text-error span12 center call_count"
      end

      calls_count_string = pluralize(calls_count, "Pending Call")
      PrivatePub.publish_to("/calls/new", closed: true, row_id: @call.id, call_count: calls_count_string, call_class: status)
      redirect_to user_path(current_user), notice: "Call closed"
    else
      render :edit
    end
  end

  def edit
    @call = Call.find params[:id]
    unless @call.status == "pending"
      redirect_to user_path(current_user), alert: "This call has already been closed!"
    end
    if @call.person
      @disabled = true
      @person = @call.person
    else
      @disabled = false
      @person = Person.new(person_mobile: @call.person_mobile)
    end
  end
end
