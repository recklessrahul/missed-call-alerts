// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require bootstrap-datepicker
//= require private_pub
//= require dataTables/jquery.dataTables
//= require dataTables/jquery.dataTables.bootstrap
//= require_tree .

$(document).ready(function(){
  $(".datepicker").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
  });
  var channels = $(".channel-data").data("call");
  var current_call_count = parseInt($(".call-count").data("count"),10);
  channels.forEach(function(element, index) {
    PrivatePub.subscribe(element, function(data, channel) {
      if(data.closed == true) {
        --current_call_count;
        var row_id = "tr.row_" + data.row_id;
        console.log("Closing " + row_id);
        console.log(current_call_count);
        $(row_id).remove();
        $("div.call_count").text(current_call_count + " Pending");
        $("div.call_count").removeClass().addClass(data.call_class);
      }
      else {
        ++current_call_count;
        console.log(current_call_count);
        $(".table_body").prepend(data.html);
        $("div.call_count").text(current_call_count + " Pending");
        $("div.call_count").removeClass().addClass(data.call_class);
        alert(data.alert);
      }
    });
  });
  $('#datatables').dataTable({
    "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
    "sPaginationType": "bootstrap",
    "aaSorting": [[ 5, "desc" ]]
  });
});
