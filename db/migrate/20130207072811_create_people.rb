class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :person_name
      t.string :person_mobile
      t.string :institution_name

      t.timestamps
    end
  end
end
