class AddPersonIdToCalls < ActiveRecord::Migration
  def change
    add_column :calls, :person_mobile, :string
    add_index :calls, :person_mobile
  end
end
