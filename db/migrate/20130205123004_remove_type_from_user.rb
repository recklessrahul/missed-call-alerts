class RemoveTypeFromUser < ActiveRecord::Migration
  def up
    remove_column :calls, :type
    add_column :calls, :call_type, :string
  end

  def down
  end
end
