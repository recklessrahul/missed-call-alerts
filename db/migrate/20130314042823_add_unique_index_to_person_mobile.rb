class AddUniqueIndexToPersonMobile < ActiveRecord::Migration
  def change
    add_index :people, :person_mobile, :unique => true
  end
end
