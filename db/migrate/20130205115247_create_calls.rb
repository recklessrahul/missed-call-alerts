class CreateCalls < ActiveRecord::Migration
  def change
    create_table :calls do |t|
      t.string :type
      t.string :from
      t.string :status
      t.text :remarks

      t.timestamps
    end
  end
end
