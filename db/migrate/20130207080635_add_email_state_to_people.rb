class AddEmailStateToPeople < ActiveRecord::Migration
  def change
    add_column :people, :person_email, :string
    add_column :people, :person_state, :string
  end
end
