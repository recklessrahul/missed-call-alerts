require 'faker'
namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    make_people
    make_calls
  end

  task fake_calls: :environment do
    make_calls
  end
end

def make_people
  10.times do |p|
    name = Faker::Name.name
    number = Faker::PhoneNumber.phone_number
    email = Faker::Internet.email
    institution = ["FTL", "TMFL", "WSG", "PNB", "HSBC", "TCS", "Infy", "Satyam"].shuffle.first
    state = "Maharashtra"
    product = ["engineer", "xpress"].shuffle.first
    Person.create(person_name: name, person_email: email, person_state: state, person_mobile: number, institution_name: institution)
  end
end

def make_calls
  Person.all.each do |p|
    uri = URI.parse("http://localhost:3000/log_a_call")
    id = [1..10].shuffle.first
    Net::HTTP.post_form(uri, {"mobile" => p.person_mobile, "type" => ["engineer", "xpress"].shuffle.first})
  end
end

